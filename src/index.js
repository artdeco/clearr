/**
 * Clears the carriage return like the terminal would.
 * @param {string} string The string with the `\r`.
 * @example
 * const res = clearR('...\r..?\r.!')
 * // output: .!?
 */
const clearR = (string) => {
  let odd = true
  const st = string.split(/(\r?\n)/).reduce((acc, l, i, a) => {
    odd = !odd
    if (odd) return acc
    const r = l.split('\r')
    const t = r.reduce((ac, current, j) => {
      if (!j) return ac
      const { length } = current
      const after = ac.slice(length)
      return `${current}${after}`
    }, r[0])
    const sep = a[i+1] || ''
    return `${acc}${t}${sep}`
  }, '')
  return st
}

export default clearR