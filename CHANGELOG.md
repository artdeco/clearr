## 8 April 2020

### [1.1.0](https://gitlab.com/artdeco/clearr/compare/v1.0.0...v1.1.0)

- [license] Update to _AGPL-3.0_.
- [package] Move to _GitLab_.

## 1 April 2019

### [1.0.0](https://gitlab.com/artdeco/clearr/compare/v0.0.0-pre...v1.0.0)

- [package] Initial publish.

### 0.0.0

- Create `clearr` with _[`My New Package`](https://mnpjs.org)_
- [repository]: `src`, `test`