<div align="center">

# Clearr

%NPM: clearr%
<pipeline-badge />
</div>

`clearr` Updates The String To Remove `\r` (Carriage Return) Just Like The Terminal Would Do.

```sh
yarn add clearr
npm i clearr
```

## Table Of Contents

%TOC%

%~%