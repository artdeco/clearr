<div align="center">

# Clearr

[![npm version](https://badge.fury.io/js/clearr.svg)](https://www.npmjs.com/package/clearr)
<a href="https://gitlab.com/artdeco/clearr/-/commits/master">
  <img src="https://gitlab.com/artdeco/clearr/badges/master/pipeline.svg" alt="Pipeline Badge">
</a>
</div>

`clearr` Updates The String To Remove `\r` (Carriage Return) Just Like The Terminal Would Do.

```sh
yarn add clearr
npm i clearr
```

## Table Of Contents

- [Table Of Contents](#table-of-contents)
- [API](#api)
- [`clearr(string: string): string`](#clearrstring-string-string)
- [Copyright & License](#copyright--license)

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>

## API

The package is available by importing its default function:

```js
import clearr from 'clearr'
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/1.svg?sanitize=true">
</a></div>

## <code><ins>clearr</ins>(</code><sub><br/>&nbsp;&nbsp;`string: string,`<br/></sub><code>): <i>string</i></code>

Clears the carriage return like the terminal would.

```js
/* alanode example/ */
import clearR from 'clearr'

const res = clearR('...\r..?\r.!')
console.log(res)
```
```
.!?
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/2.svg?sanitize=true">
</a></div>

## Copyright & License

GNU Affero General Public License v3.0

<table>
  <tr>
    <th>
      <a href="https://www.artd.eco">
        <img width="100" src="https://gitlab.com/uploads/-/system/group/avatar/7454762/artdeco.png"
          alt="Art Deco">
      </a>
    </th>
    <th>© <a href="https://www.artd.eco">Art Deco™</a>   2020</th>
    <th><a href="LICENSE"><img src=".documentary/agpl-3.0.svg" alt="AGPL-3.0"></a></th>
  </tr>
</table>

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/-1.svg?sanitize=true">
</a></div>